#!/usr/bin/env bash
FILE_NAME="mongodump_$(date '+%Y%m%d_%H%M%S')"
TAR_NAME="$FILE_NAME.tar.gz"

BLOB_NAME="$AZURE_PATH/$TAR_NAME"
if [ ${BLOB_NAME:0:1} = "/" ]; then BLOB_NAME="${BLOB_NAME:1}"; fi

mongodump --uri="$MONGO_URI" --out="$FILE_NAME"
tar -czvf "$TAR_NAME" "$FILE_NAME"
az storage blob upload -c "$CONTAINER" -f "$TAR_NAME" -n "$BLOB_NAME"