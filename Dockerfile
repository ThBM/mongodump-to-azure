FROM mongo:4
WORKDIR /app
CMD ["./mongodump-to-azure.sh"]
RUN apt-get update && \
    apt-get install -y curl && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash
COPY mongodump-to-azure.sh ./
RUN chmod a+x mongodump-to-azure.sh
